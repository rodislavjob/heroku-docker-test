FROM nginx

COPY index.html /usr/share/nginx/html
COPY run.sh /run.sh

CMD ["bash", "/run.sh"]